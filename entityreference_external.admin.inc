<?php

/**
 * Admin settings form.
 */
function entityreference_external_admin_settings() {
  $form = array();
  $form['#validate'][] = 'entityreference_external_admin_settings_validate';

  $src_type = 'text';
  $dst_type = 'entityreference';

  // Get list of all fields, use the lighter field_info_field_map()
  // if available (Drupal >= 7.22).
  $fields = function_exists('field_info_field_map') ? field_info_field_map() : field_info_fields();
  ksort($fields);

  $field_map = $src_fields = array();
  $entity_info = entity_get_info();

  foreach ($fields as $field_name => $field) {
    $type = $field['type'];

    // Exclude fields of types we don't need.
    if (!in_array($type, array($src_type, $dst_type))) {
      continue;
    }

    // Populate list of all potential source fields.
    if ($type == $src_type) {
      $src_fields[$field_name] = $field_name;
    }

    // Populate list of per-bundle fields.
    foreach ($field['bundles'] as $entity_name => $bundles) {
      foreach ($bundles as $bundle_name) {
        $field_instance = field_info_instance($entity_name, $field_name, $bundle_name);
        $field_map[$entity_name][$bundle_name][$type][$field_name] = $field_instance['label'];
      }
    }
  }

  // Keep only items with at least one src and one dst field.
  $field_map = array_filter(
    array_map(function ($bundles) {
      return array_filter($bundles, function ($types) {
        return count($types) == 2;
      });
    }, $field_map));


  $form['entityreference_external_local_id_field'] = array(
    '#type' => 'select',
    '#title' => t('External ID field'),
    '#default_value' => variable_get('entityreference_external_local_id_field', ''),
    '#description' => t('The field where the external ID of an item is stored.'),
    '#options' => $src_fields,
  );

  $mapping = variable_get('entityreference_external_fields_mapping', array());

  $form['entityreference_external_fields_mapping'] = array(
    '#type' => 'fieldset',
    '#title' => t('Fields mapping'),
    '#collapsible' => FALSE,
    '#tree' => TRUE,
  );

  foreach ($field_map as $entity_name => $bundles) {
    $form['entityreference_external_fields_mapping'][$entity_name] = array(
      '#type' => 'fieldset',
      '#title' => $entity_info[$entity_name]['label'],
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
      '#tree' => TRUE,
    );

    foreach ($bundles as $bundle_name => $types) {
      $form['entityreference_external_fields_mapping'][$entity_name][$bundle_name] = array(
        '#type' => 'fieldset',
        '#title' => $entity_info[$entity_name]['bundles'][$bundle_name]['label'],
        '#collapsible' => TRUE,
        '#collapsed' => TRUE,
        '#tree' => TRUE,
      );

      $rows = min(count($types[$src_type]), count($types[$dst_type]));

      for ($i = 0; $i < $rows; $i++) {

        // Find default value.
        $default = !empty($mapping[$entity_name][$bundle_name][$i]) ? $mapping[$entity_name][$bundle_name][$i] : array('src' => '', 'dst' => '');

        $row = array(
          '#tree' => TRUE,
          '#prefix' => $i == 0 ? '<table><tr>' : '<tr>',
          '#suffix' => $i == $rows - 1 ? '</tr></table>' : '</tr>',
        );
        $row['src'] = array(
          '#type' => 'select',
          '#title' => t('Source field'),
          '#options' => $types[$src_type],
          '#default_value' => $default['src'],
          '#empty_value' => '',
          '#description' => t('Field were is stored the external ID of items to relate'),
          '#tree' => TRUE,
          '#prefix' => '<td>',
          '#suffix' => '</td>',
        );
        $row['dst'] = array(
          '#type' => 'select',
          '#title' => t('Destination field'),
          '#options' => $types[$dst_type],
          '#default_value' => $default['dst'],
          '#empty_value' => '',
          '#description' => t('Field were will be stored the entityreferences'),
          '#tree' => TRUE,
          '#prefix' => '<td>',
          '#suffix' => '</td>',
        );

        $form['entityreference_external_fields_mapping'][$entity_name][$bundle_name][$i] = $row;
      }
    }
  }

  return system_settings_form($form);
}

/**
 * Form submit handler for admin settings.
 */
function entityreference_external_admin_settings_validate($form, &$form_state) {
  $map = &$form_state['values']['entityreference_external_fields_mapping'];

  /**
   * Recursively remove empty form values.
   */
  $filter = function (&$item) use (&$filter) {
    if (!array_key_exists('src', $item) && !array_key_exists('dst', $item)) {
      // Recurse.
      $item = array_filter($item, $filter);
      return !empty($item);
    }

    return !empty($item['src']) && !empty($item['dst']);
  };

  $map = array_filter($map, $filter);
}
